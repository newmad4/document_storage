from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    # Service Field
    ip_address = models.GenericIPAddressField(default='192.168.0.1')

    class Meta:
        unique_together = ('email',)
