from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'documents_storage.apps.user'
