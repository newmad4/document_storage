from braces.views import LoginRequiredMixin


class RestrictToOwnerMixin(LoginRequiredMixin):
    def get_queryset(self):
        queryset = super(RestrictToOwnerMixin, self).get_queryset()
        if self.request.user.is_staff or self.request.user.is_superuser:
            return queryset
        queryset = queryset.filter(user=self.request.user)
        return queryset
