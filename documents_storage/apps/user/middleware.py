class SaveIpAddressMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    @staticmethod
    def save_ip(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request.META.get('REMOTE_ADDR')
        user = request.user
        if user.ip_address != ip:
            user.ip_address = ip
            user.save()

    def __call__(self, request):
        response = self.get_response(request)
        if not request.user.is_anonymous:
            self.save_ip(request)
        return response
