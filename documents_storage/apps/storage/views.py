from django.views.generic import ListView, CreateView, UpdateView
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin, UserPassesTestMixin

from documents_storage.apps.storage.models import Source, Document
from documents_storage.apps.user.mixins import RestrictToOwnerMixin


class SourceListView(LoginRequiredMixin, ListView):
    model = Source
    template_name = "storage/source_list.html"
    content_type = "text/html"
    queryset = Source.objects.all().order_by('id')


class SourceCreateView(LoginRequiredMixin, StaffuserRequiredMixin, CreateView):
    model = Source
    template_name = "storage/source_detail.html"
    content_type = "text/html"
    fields = '__all__'

    def get_form(self, form_class=None):
        form = super(SourceCreateView, self).get_form(form_class)
        form.fields['date_field'].widget.attrs.update({'class': 'datepicker'})
        return form


class SourceUpdateView(LoginRequiredMixin, StaffuserRequiredMixin, UpdateView):
    model = Source
    template_name = "storage/source_detail.html"
    content_type = "text/html"
    fields = '__all__'


class DocumentListView(LoginRequiredMixin, ListView):  # RestrictToOwnerMixin,
    model = Document
    template_name = "storage/document_list.html"
    content_type = "text/html"
    queryset = Document.objects.all().order_by('id')


class DocumentCreateView(LoginRequiredMixin, CreateView):  # RestrictToOwnerMixin,
    model = Document
    template_name = "storage/document_detail.html"
    content_type = "text/html"
    fields = '__all__'

    def get_form(self, form_class=None):
        form = super(DocumentCreateView, self).get_form(form_class)
        form.fields['date_field'].widget.attrs.update({'class': 'datepicker'})
        return form


class DocumentUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Document
    template_name = "storage/document_detail.html"
    content_type = "text/html"
    fields = '__all__'
