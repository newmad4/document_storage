from django import forms
from .models import Source, Document


class SourceForm(forms.ModelForm):
    class Meta:
        model = Source
        fields = ['id']


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['id']
