from django.contrib import admin

from documents_storage.apps.storage.models import Source, Document


admin.site.register(Source)
admin.site.register(Document)
