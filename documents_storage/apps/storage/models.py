import uuid

from django.db import models
from django.utils import timezone
from django.utils.datetime_safe import datetime

from documents_storage.apps.user.models import User


class Source(models.Model):
    sid = models.UUIDField(default=uuid.uuid4, editable=False)  # maybe models.SlugField?
    name = models.CharField(blank=True, null=False, max_length=300)
    url = models.URLField()

    def __str__(self):
        return self.name


class Document(models.Model):
    title = models.CharField(blank=True, null=False, max_length=300)
    text = models.TextField(max_length=3000)
    url = models.URLField()
    created = models.DateTimeField(blank=True)
    source = models.ForeignKey(Source)
    created_at = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    number_of_edits = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.title

    def is_editable(self):
        this_time = datetime.now(timezone.utc)
        if (this_time - self.created_at).seconds <= 60 * 60:
            return True
        else:
            return False

    def increment_number_of_edits(self):
        self.number_of_edits += 1

    class Meta:
        unique_together = ('title', 'text')
