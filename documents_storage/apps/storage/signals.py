from django.db.models.signals import post_save
from django.dispatch import receiver

from documents_storage.apps.storage.models import Document


@receiver(post_save, sender=Document)
def update_document(sender, instance, created, **kwargs):
    if not created and instance.is_editable():
        instance.increment_number_of_edits()
