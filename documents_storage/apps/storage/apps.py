from django.apps import AppConfig


class StorageConfig(AppConfig):
    name = 'documents_storage.apps.storage'

    def ready(self):
        import documents_storage.apps.storage.signals  # noqa
