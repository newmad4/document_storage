from django.conf.urls import url

from documents_storage.apps.storage.views import SourceListView, SourceCreateView, SourceUpdateView, DocumentListView, \
    DocumentCreateView, DocumentUpdateView

urlpatterns = [
    url(r'^source', SourceListView.as_view(), name='source_list'),
    url(r'^document', DocumentListView.as_view(), name='document_list'),
    url(r'^source/create/', SourceCreateView.as_view(), name='source_create'),
    url(r'^source/(?P<pk>[0-9]+)/update/', SourceUpdateView.as_view(), name='source_update'),
    url(r'^document/create/', DocumentCreateView.as_view(), name='document_create'),
    url(r'^document/(?P<pk>[0-9]+)/update/', DocumentUpdateView.as_view(), name='document_update'),
]
